let student = {
  name: prompt("Ім'я?"),
  lastName: prompt('Прізвище?'),
  tabel: {
    Математика: null,
    ['Англійска мова']: null,
    Біологія: null,
    ['Фізична культура']: null,
    Географія: null,
    Геометрія: null,
    Астрономія: null,
    Фізика: null,
    Мистецтвознавство: null,
    Геологія: null,
    Ботаніка: null,
    Екологія: null,
    ['Французька мова']: null,
    Тригонометрія: null,
    Логіка: null,
    Соціологія: null,
  }
}

for(let key in student.tabel) {
    student.tabel[key] = prompt(`${key}: оцінка?`);
    if(student.tabel[key] == null) break;
}

for(let key in student.tabel){
  if(student.tabel[key] == null){
    delete student.tabel[key];
  }
}

console.log(student);

let badGradesCounter = 0;
for (let key in student.tabel) {
  if(student.tabel[key] < 3 && student.tabel[key] != null) {
    badGradesCounter++;
  }
}

if(badGradesCounter == 0){
  console.log('Студента переведено на наступний курс');
} else {
  console.log('Кількість поганих оцінок: ' + badGradesCounter);
}

let sum = 0;
for(let key in student.tabel) {
  sum += +student.tabel[key];
}

let gradedSubjects = 0
for(let key in student.tabel) {
  if(student.tabel[key] > 0) {
    gradedSubjects++;
  }
}

let averageGrade = sum / gradedSubjects;

console.log('Середній бал: ' + averageGrade);

if (averageGrade > 7){
  console.log('Студенту призначено стипендію');
}